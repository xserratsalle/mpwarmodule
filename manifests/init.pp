class mpwarmodule {
    # Puppet manifest for my PHP dev machine

    # Edit local /etc/hosts files to resolve some hostnames used on your application.
    host { 'localhost':
      ensure => 'present',
      target => '/etc/hosts',
      ip => '127.0.0.1',
      host_aliases => ['mysql1', 'memcached1']
    }

    # Miscellaneous packages.
    $misc_packages = ['vim-enhanced','telnet','zip','unzip','git','screen','libssh2','libssh2-devel', 'gcc', 'gcc-c++', 'autoconf', 'automake']

    package { $misc_packages: ensure => latest }

    #include iptables

    class{'apache':}

    apache::vhost { 'la-salle.dev':
          port          => '80',
          docroot       => '/www',
    }

    # MYSQL
    $databases = {
      'lasalleDB' => {
        ensure  => 'present',
        charset => 'utf8',
      },
    }

    class { '::mysql::server':
      root_password    => 'root',
      databases => $databases,
    }

    #MYSQL ADD FILE .my.cnf

    file { '/home/vagrant/.my.cnf':
      ensure => present,
      require => File["/etc/my.cnf"],
      content => '
      [CLIENT]
      user=root
      password=root';
    }

    # PHP
    $php_version = '56'

    # remi_php55 requires the remi repo as well
    if $php_version == '55' {
      $yum_repo = 'remi-php55'
      include ::yum::repo::remi_php55
    }
    # remi_php56 requires the remi repo as well
    elsif $php_version == '56' {
      $yum_repo = 'remi-php56'
      ::yum::managed_yumrepo { 'remi-php56':
        descr          => 'Les RPM de remi pour Enterpise Linux $releasever - $basearch - PHP 5.6',
        mirrorlist     => 'http://rpms.famillecollet.com/enterprise/$releasever/php56/mirror',
        enabled        => 1,
        gpgcheck       => 1,
        gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi',
        gpgkey_source  => 'puppet:///modules/yum/rpm-gpg/RPM-GPG-KEY-remi',
        priority       => 1,
      }
    }
    # version 5.4
    elsif $php_version == '54' {
      $yum_repo = 'remi'
      include ::yum::repo::remi
    }

    class { 'php':
      version => 'latest',
      require => Yumrepo[$yum_repo]
    }

    php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap', 'pdo', 'mcrypt' ]: }

    #Install Composer
    class { 'composer':
      command_name => 'composer',
      target_dir   => '/usr/local/bin'
    }

    # memcached
    include memcached

    #Servicio para el control de la hora en el server
    class{'timezone':
		timezone => 'Europe/Madrid',
	  }
    class { '::ntp':
        servers => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
        package_ensure => 'latest'
    }
	

    # Modulo ejercicio.
    class{'ejercicio':} 
}
